﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_homework_9_PatternPrototype.Interfaces
{
    public interface IMyClonable<T>
    {
        T MyClone();
    }
}
