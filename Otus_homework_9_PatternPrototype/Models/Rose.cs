﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus_homework_9_PatternPrototype.Interfaces;

namespace Otus_homework_9_PatternPrototype.Models
{
    public class Rose : Flower, ICloneable
    {
        public string sort;

        public Rose(string name, string color, string sort) : base(name, color)
        {

        }

        public override Rose MyClone()
        {
            return new Rose(name, color, sort);
        }

        public override object Clone()
        {
            return MyClone();
        }
    }
}
