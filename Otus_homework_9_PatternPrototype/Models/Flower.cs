﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus_homework_9_PatternPrototype.Interfaces;

namespace Otus_homework_9_PatternPrototype.Models
{
    public class Flower : Plant, ICloneable
    {
        public string color;

        public Flower(string name, string color) : base(name)
        {
            this.color = color;
        }

        public override Flower MyClone()
        {
            return new Flower(name, color);
        }

        public override object Clone()
        {
            return MyClone();
        }
    }
}
