﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus_homework_9_PatternPrototype.Interfaces;

namespace Otus_homework_9_PatternPrototype.Models
{
    public class Plant : IMyClonable<Plant>, ICloneable
    {
        public string name;

        public Plant (string name)
        {
            this.name = name;
        }

        public virtual Plant MyClone()
        {
            return new Plant (name);
        }

        public virtual object Clone()
        {
            return MyClone();
        }
    }
}
