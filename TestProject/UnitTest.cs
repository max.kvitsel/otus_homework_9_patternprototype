using Otus_homework_9_PatternPrototype.Models;
using System;
using Xunit;

namespace TestProject
{
    public class UnitTest
    {
        [Fact]
        public void PlantMyClone()
        {
            // Arrange
            string name = "SomeFlower";
            Plant plant = new Plant(name);
            // Act
            Plant plantClone = plant.MyClone();
            // Assert
            Assert.Equal(plant.name, plantClone.name);
            Assert.NotSame(plant, plantClone);
        }

        [Fact]
        public void PlantClone()
        {
            // Arrange
            string name = "SomeFlower";
            Plant plant = new Plant(name);
            // Act
            Plant plantClone = (Plant)plant.Clone();
            // Assert
            Assert.Equal(plant.name, plantClone.name);
            Assert.NotSame(plant, plantClone);
        }

        [Fact]
        public void FlowerMyClone()
        {
            // Arrange
            string name = "SomeFlower";
            string color = "White";
            Flower flower = new Flower(name, color);
            // Act
            Flower flowerClone = flower.MyClone();
            // Assert
            Assert.Equal(flower.name, flowerClone.name);
            Assert.Equal(flower.color, flowerClone.color);
            Assert.NotSame(flower, flowerClone);
        }

        [Fact]
        public void FlowerClone()
        {
            // Arrange
            string name = "SomeFlower";
            string color = "White";
            Flower flower = new Flower(name, color);
            // Act
            Flower flowerClone = (Flower)flower.Clone();
            // Assert
            Assert.Equal(flower.name, flowerClone.name);
            Assert.Equal(flower.color, flowerClone.color);
            Assert.NotSame(flower, flowerClone);
        }

        [Fact]
        public void RoseMyClone()
        {
            // Arrange
            string name = "SomeFlower";
            string color = "White";
            string sort = "Tea";
            Rose rose = new Rose(name, color, sort);
            // Act
            Rose roseClone = rose.MyClone();
            // Assert
            Assert.Equal(rose.name, roseClone.name);
            Assert.Equal(rose.color, roseClone.color);
            Assert.Equal(rose.sort, roseClone.sort);
            Assert.NotSame(rose, roseClone);
        }

        [Fact]
        public void RoseClone()
        {
            // Arrange
            string name = "SomeFlower";
            string color = "White";
            string sort = "Tea";
            Rose rose = new Rose(name, color, sort);
            // Act
            Rose roseClone = (Rose)rose.Clone();
            // Assert
            Assert.Equal(rose.name, roseClone.name);
            Assert.Equal(rose.color, roseClone.color);
            Assert.Equal(rose.sort, roseClone.sort);
            Assert.NotSame(rose, roseClone);
        }


    }
}
